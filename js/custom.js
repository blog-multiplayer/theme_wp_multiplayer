
	
(function($) {
	
        // You pass-in jQuery and then alias it with the $-sign
        // So your internal code doesn't change
    $('.home-slider__wrapper').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 2000,
	  centerMode: true,
	});
	
	$('.carousel__wrapper').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 2000,
	  centerMode:true,
	  centerPadding:"50px",
	});
	
	
})(jQuery);


