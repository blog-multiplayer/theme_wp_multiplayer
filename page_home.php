<?php
/**
* Template Name: page home
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();
?>

    <main class="container-fluid home-content">
		
		<div class="row">
		
			<div class="col-xs-12 col-sm-12 col-md-12 home-slider">
		
				<div class="home-slider__wrapper">
				
					<?php
					
						if (function_exists('the_field')) {
							
							$slider_imagen_1		 = get_field('slider_imagen_1');
							$slider_imagen_2		 = get_field('slider_imagen_2');
							$slider_imagen_3		 = get_field('slider_imagen_3');
							$slider_imagen_mobile_1  = get_field('slider_imagen_mobile_1');
							$slider_imagen_mobile_2  = get_field('slider_imagen_mobile_2');
							$slider_imagen_mobile_3  = get_field('slider_imagen_mobile_3');
							$slider_link_1           = get_field('slider_link_1');
							$slider_link_2           = get_field('slider_link_2');
							$slider_link_3           = get_field('slider_link_3');
							
							echo "
							      <div>
									<a href='$slider_link_1' target='_blank'>
							        <img src='$slider_imagen_1' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_imagen_mobile_1' class='img-fluid d-block d-sm-none'/>
									</a>
								  </div>
								  
							     <div>
							        <a href='$slider_link_2' target='_blank'>
							        <img src='$slider_imagen_2' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_imagen_mobile_2' class='img-fluid d-block d-sm-none'/>
									</a>
								  </div>
								  
							      <div>
							        <a href='$slider_link_3' target='_blank'>
							        <img src='$slider_imagen_3' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_imagen_mobile_3' class='img-fluid d-block d-sm-none'/>
									</a>
								  </div>
								 ";
								  
						} else {
							
							echo "No existe la funcion the_field()<br/>";
							
						}
					
					?>
					
				 
				  <!--
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  -->
					  
				</div>
			
			</div>
			
			<div class="col-xs-8 col-sm-8 col-md-8 home-latest-news mt-4">
			
				<h3>Lo Ultimo</h3>

				<div class="row">

					<?php

					$args = array( 'numberposts' => '4');
					$recent_posts = wp_get_recent_posts( $args );

					foreach( $recent_posts as $recent ){
						echo  '<div class="col-xs-6 col-sm-6 col-md-6 mt-4">'.
						      '<a href="'.get_permalink($recent["ID"]).'">'.
						      '<img src="'.get_the_post_thumbnail_url($recent["ID"]).'" class="img-fluid" />'.
							  '</a>'.
							  '</div>';
					}

					?>
					
					
				</div>

			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 home-side-bar mt-4 ">
				
				<?php
				
				echo get_search_form();
				
				?>
				
				<div class="row">

					<?php
					
						if (function_exists('the_field')) {
							
							$barra_lateral_imagen_1		   = get_field('barra_lateral_imagen_1');
							$barra_lateral_imagen_mobile_1 = get_field('barra_lateral_imagen_mobile_1');
							$barra_lateral_link_1          = get_field('barra_lateral_link_1');

							$barra_lateral_imagen_2		   = get_field('barra_lateral_imagen_2');
							$barra_lateral_imagen_mobile_2 = get_field('barra_lateral_imagen_mobile_2');
							$barra_lateral_link_2          = get_field('barra_lateral_link_2');
							
							echo "
									<div class='col-xs-12 col-sm-12 col-md-12 mt-4'>
										<a href='$barra_lateral_link_1' target='_blank'>
										<img src='$barra_lateral_imagen_1' class='img-fluid d-none d-sm-block'/>
										<img src='$barra_lateral_imagen_mobile_1' class='img-fluid d-block d-sm-none'/>
										</a>
									</div>

									<div class='col-xs-12 col-sm-12 col-md-12 mt-4'>
										<a href='$barra_lateral_link_2' target='_blank'>
										<img src='$barra_lateral_imagen_2' class='img-fluid d-none d-sm-block'/>
										<img src='$barra_lateral_imagen_mobile_2' class='img-fluid d-block d-sm-none'/>
										</a>
									</div>
					
								";
								  
						} else {
							
							echo "No existe la funcion the_field()<br/>";
							
						}
					
					?>

					<!--
				
					<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
					
						<img src="https://via.placeholder.com/400x300" class="img-fluid" />
					
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
					
						<img src="https://via.placeholder.com/400x600" class="img-fluid" />
					
					</div>

					-->
				
				</div>
			
			</div>
		
			<div class="col-xs-12 col-sm-12 col-md-12 mt-4 mb-4 home-mix-carousel">
			
				<h2 class="mb-4">Le tenemos de todo</h2>
			
			    <!--
				<div class="carousel__wrapper">
				
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
					  
				</div>
				-->

				<div class="carousel__wrapper">

				<?php
					
					if (function_exists('the_field')) {
						
						$carrusel_imagen_1 = get_field('carrusel_imagen_1');
						$carrusel_imagen_2 = get_field('carrusel_imagen_2');
						$carrusel_imagen_3 = get_field('carrusel_imagen_3');
						$carrusel_imagen_4 = get_field('carrusel_imagen_4');
						$carrusel_imagen_5 = get_field('carrusel_imagen_5');
						$carrusel_imagen_6 = get_field('carrusel_imagen_6');

						$carrusel_link_1 = get_field('carrusel_link_1');
						$carrusel_link_2 = get_field('carrusel_link_2');
						$carrusel_link_3 = get_field('carrusel_link_3');
						$carrusel_link_4 = get_field('carrusel_link_4');
						$carrusel_link_5 = get_field('carrusel_link_5');
						$carrusel_link_6 = get_field('carrusel_link_6');
						
						echo "
						      <div>
						        <a href='$carrusel_link_1' target='_blank'>
								<img src='$carrusel_imagen_1' class='img-fluid'/>
								</a>
							  </div>

							  <div>
						        <a href='$carrusel_link_2' target='_blank'>
								<img src='$carrusel_imagen_2' class='img-fluid'/>
								</a>
							  </div>

							  <div>
						        <a href='$carrusel_link_3' target='_blank'>
								<img src='$carrusel_imagen_3' class='img-fluid'/>
								</a>
							  </div>

							  <div>
						        <a href='$carrusel_link_4' target='_blank'>
								<img src='$carrusel_imagen_4' class='img-fluid'/>
								</a>
							  </div>

							  <div>
						        <a href='$carrusel_link_5' target='_blank'>
								<img src='$carrusel_imagen_5' class='img-fluid'/>
								</a>
							  </div>

							  <div>
						        <a href='$carrusel_link_6' target='_blank'>
								<img src='$carrusel_imagen_6' class='img-fluid'/>
								</a>
							  </div>

							  ";
							  
					} else {
						
						echo "No existe la funcion the_field()<br/>";
						
					}
				
				?>

				</div>
			
			</div>
		
		</div>
	
	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();
