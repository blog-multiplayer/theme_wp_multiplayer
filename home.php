<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package multiplayer
 */

get_header();
?>

	<main class="container-fluid home-content">
		
		<div class="row">
		
			<div class="col-xs-12 col-sm-12 col-md-12 home-slider">
		
				<div class="home-slider__wrapper">
				
					<?php
					
						if (function_exists('the_field')) {
							
							$slider_1 		 = get_field('slider_1');
							$slider_2 		 = get_field('slider_2');
							$slider_3 		 = get_field('slider_3');
							$slider_mobile_1 = get_field('slider_mobile_1');
							$slider_mobile_2 = get_field('slider_mobile_2');
							$slider_mobile_3 = get_field('slider_mobile_3');
							
							echo "<div>
							        <img src='$slider_1' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_mobile_1' class='img-fluid d-block d-sm-none'/>
								  </div>";
								  
							echo "<div>
							        <img src='$slider_2' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_mobile_2' class='img-fluid d-block d-sm-none'/>
								  </div>";
								  
							echo "<div>
							        <img src='$slider_3' class='img-fluid d-none d-sm-block'/>
									<img src='$slider_mobile_3' class='img-fluid d-block d-sm-none'/>
								  </div>";
								  
						} else {
							
							echo "No existe la funcion the_field()<br/>";
							
						}
					
					?>
					
				 
				  <!--
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/1800x400" class="img-fluid"/>
				  </div>
				  -->
					  
				</div>
			
			</div>
			
			<div class="col-xs-8 col-sm-8 col-md-8 home-latest-news mt-4">
			
				<h3>Lo Ultimo</h3>

				<div class="row">

					<?php

					$args = array( 'numberposts' => '4');
					$recent_posts = wp_get_recent_posts( $args );

					foreach( $recent_posts as $recent ){
						echo '<div class="col-xs-6 col-sm-6 col-md-6 mt-4">'.
						      '<a href="' . get_permalink($recent["ID"]) . '">'.
						      '<img src="'.get_the_post_thumbnail_url($recent["ID"]).'" class="img-fluid" />'.
							  '</a>'.
							  '</div>';
					}

					?>
					
					
				</div>

			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 home-side-bar mt-4 ">
				
				<?php
				
				echo get_search_form();
				
				?>
				
				<div class="row">
				
					<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
					
						<img src="https://via.placeholder.com/400x300" class="img-fluid" />
					
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
					
						<img src="https://via.placeholder.com/400x600" class="img-fluid" />
					
					</div>
				
				</div>
			
			</div>
		
			<div class="col-xs-12 col-sm-12 col-md-12 mt-4 home-mix-carousel">
			
				<h2>Le tenemos de todo</h2>
			
				<div class="carousel__wrapper">
				
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
				  
				  <div>
					<img src="https://via.placeholder.com/400x400" class="img-fluid"/>
				  </div>
					  
				</div>
			
			</div>
		
		</div>
	
	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();
